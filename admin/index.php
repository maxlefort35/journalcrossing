<?php

	session_start();

	require('log.php');

	
	if(!empty($_POST['email']) && !empty($_POST['password'])){

		require('connect.php');

		// VARIABLES
		$email 			= htmlspecialchars($_POST['email']);
		$password		= htmlspecialchars($_POST['password']);

		// ADRESSE EMAIL SYNTAXE
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {

			header('location: index.php?error=1&message=Votre adresse email est invalide.');
			exit();

		}

		// CHIFFRAGE DU MOT DE PASSE
		$password = "aq1".sha1($password."123")."25";

		// EMAIL DEJA UTILISE
		$req = $db->prepare("SELECT count(*) as numberEmail FROM user WHERE email = ?");
		$req->execute(array($email));

		while($email_verification = $req->fetch()){
			if($email_verification['numberEmail'] != 1){
				header('location: index.php?error=1&message=Impossible de vous authentifier correctement.');
				exit();
			}
		}

		// CONNEXION
		$req = $db->prepare("SELECT * FROM user WHERE email = ?");
		$req->execute(array($email));

		while($user = $req->fetch()){

			if($password == $user['password'] && $user['blocked'] == 0){ 

				$_SESSION['connect'] = 1;
				$_SESSION['email']   = $user['email'];

				if(isset($_POST['auto'])){
					setcookie('auth', $user['secret'], time() + 364*24*3600, '/', null, false, true);
				}

				header('location: index.php?success=1');
				exit();

			}
			else {

				header('location: index.php?error=1&message=Impossible de vous authentifier correctement.');
				exit();
			}

		}

	}

?>

<!DOCTYPE html>
<html>
  <head>
        <title>Journal Crossing</title>
        <html lang="fr">
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Holtwood+One+SC|Patrick+Hand|Sacramento&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="../css/styles.css">
  </head>
    
<body>
  <header>   
      <div id="logo">
            <img src="../images/logocrossing.png" alt="">
      </div>
  <div id="login-body">
            <?php if(isset($_SESSION['connect'])){  ?>
              <?php
              
              if(isset($_GET['success'])){

                echo'<div class="alert success"><span class="glyphicon glyphicon-ok"></span> Vous êtes maintenant connecté.</div>';	
              } ?>
              <a class="btn btn-danger" href="logout.php"><span class="glyphicon glyphicon-off"></span> Déconnexion</a>     
    </div>      
  </header>    
        
      <section>
        <div class="container admin">
            <div class="row">
              <h1><strong>Mon journal  </strong><a href="insert.php" class="btn btn-success btn-lg"><span class="glyphicon glyphicon-plus"></span> Ajouter</a> <a href="../index.php" class="btn btn-success btn-lg"><span class="glyphicon glyphicon-log-out"></span> Revenir au site</a></h1>
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>Nom</th>
                      <th>Description</th>
                      <th>Date</th>
                      <th>Catégorie</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php
                        require 'database.php';
                        $db = Database::connect();
                        $statement = $db->query('SELECT items.id, items.name, items.description, items.date, categories.name AS category FROM items LEFT JOIN categories ON items.category = categories.id ORDER BY items.id DESC');
                        while($item = $statement->fetch()) 
                        {
                            echo '<tr>';
                            echo '<td>'. $item['name'] . '</td>';
                            echo '<td>'. $item['description'] . '</td>';
                            echo '<td>'. $item['date'] . '</td>';
                            echo '<td>'. $item['category'] . '</td>';
                            echo '<td width=300>';
                            echo '<a class="btn btn-default2" href="view_admin.php?id='.$item['id'].'"><span class="glyphicon glyphicon-eye-open"></span> Voir</a>';
                            echo ' ';
                            echo '<a class="btn btn-primary" href="update.php?id='.$item['id'].'"><span class="glyphicon glyphicon-pencil"></span> Modifier</a>';
                            echo ' ';
                            echo '<a class="btn btn-danger" href="delete.php?id='.$item['id'].'"><span class="glyphicon glyphicon-remove"></span> Supprimer</a>';
                            echo '</td>';
                            echo '</tr>';
                        }
                        Database::disconnect();
                      ?>
                  </tbody>
                </table>
            </div>
        </div>

          <?php } else {?>
            <h2>S'identifier</h2>
          <?php if(isset($_GET['error'])) {

              if(isset($_GET['message'])) {
                echo'<div class="alert error">'.htmlspecialchars($_GET['message']).'</div>';
              }

            } ?>

            <form method="post" action="index.php">
              <input type="email" name="email" placeholder="Votre adresse email" required /> <br>
              <input type="password" name="password" placeholder="Mot de passe" required /> <br>
              <button id="identif" type="submit">S'identifier</button> <br><br>
              <label id="option"><input type="checkbox" name="auto" checked />Se souvenir de moi</label>              
            </form>	
            <p class="grey">Première visite sur le Journal Crossing ? <a href="inscription.php">Inscrivez-vous</a>.</p>
			
	</section>
            <a href="../index.php"><span class="glyphicon glyphicon-arrow-left"></span> Retour</a> 
	</section>		
			<?php } ?>
</body>
</html>
