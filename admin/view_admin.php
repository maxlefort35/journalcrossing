<?php
// On prolonge la session
session_start();
// On teste si la variable de session existe et contient une valeur
if(empty($_SESSION['connect'])) 
{
  // Si inexistante ou nulle, on redirige vers le formulaire de login
  header('Location: index.php');
  exit();
}
?>


<?php
    require 'database.php';

    if(!empty($_GET['id'])) 
    {
        $id = checkInput($_GET['id']);
    }
     
    $db = Database::connect();
    $statement = $db->prepare("SELECT items.id, items.name, items.description, items.date, items.image, categories.name AS category FROM items LEFT JOIN categories ON items.category = categories.id WHERE items.id = ?");
    $statement->execute(array($id));
    $item = $statement->fetch();
    Database::disconnect();

    function checkInput($data) 
    {
      $data = trim($data);
      $data = stripslashes($data);
      return $data;
    }

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Journal Crossing</title>
        <html lang="fr">
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Holtwood+One+SC|Patrick+Hand|Sacramento&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="../css/styles.css">
    </head>
    
    <body>
    <header>
        <div class="co">
            <a href="admin/index.php"><img class="cloud_co" src="../images/cloud-computing.png" alt=""></a>
        </div>
        <div id="logo">
            <img src="../images/logocrossing.png" alt="">
        </div>
        </header>         
        <div class="container">
        
            
                <div class="col-sm-12 site">
                    <div class="containered">
                        <img src="../images/cloud.png" alt="nuage">
                        <div class="centered">
                            <h4 class="apercu_date"><?php echo $item['date'];?></h4>
                        </div>
                    </div>
                    <div class="thumbnail">
                        <img src="<?php echo '../images/'.$item['image'];?>" alt="...">    
                    </div>
                    <div class="container description">
                            <p><?php echo $item['description'];?></p>
                          </div>
                          <br>                   
                    <div class="form-actions">
                        <a class="btn btn-primary view" href="index.php">
                        <span class="glyphicon glyphicon-arrow-left"></span> Retour</a>
                      </div>
                </div>
            
        </div>   
    </body>
</html>

